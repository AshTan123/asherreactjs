// Take an input from user and display on browser
import React from 'react'


class Theproductdata extends React.Component{

    // step 2: Add a constructor
    constructor(props){
        super(props);
        this.state = {value: ''};
    }


    // step 3: add handlechange
    handleChange = event => {
        this.setState({value : event.target.value});
        //this.setState({value2 : event.target.value2});
        //this.setState({value3 : event.target.value3});
        //this.setState({value4 : event.target.value4});
        //this.setState({value5 : event.target.value5});
    }

    handleChange2 = event => {
        this.setState({value2 : event.target.value2});
        
    }

    handleChange3 = event => {
        this.setState({value3 : event.target.value3});
        
    }

    handleChange4 = event => {
        this.setState({value4 : event.target.value4});
        
    }

    handleChange5 = event => {
        this.setState({value5 : event.target.value5});
        
    }


     // step 4:
     handleSubmit(event){
        //console.log('Hello ' +this.state.value);
        event.preventDefault();
        //1) display the product id
        var productID = document.getElementById("id").value;
        document.getElementById("displayid").innerHTML = productID;

        //2) display the product name
        var productName = document.getElementById("name").value;
        document.getElementById("displayname").innerHTML = productName;
       //event.preventDefault();

       //3) Display Product Category
       var productCategory = document.getElementById("category").value;
       document.getElementById("displaycategory").innerHTML = productCategory;

       //4) Display Unit Price
       var unitPrice =  document.getElementById("unitprice").value;
       document.getElementById("displayunitprice").innerHTML = unitPrice;

       //5) Display Quantity
       var quantity = document.getElementById("quantity").value;
       document.getElementById("displayquantity").innerHTML = quantity;

       // 6) Display Total
       var total = unitPrice * quantity;
       document.getElementById("displaytotal").innerHTML = total;
       document.getElementById("displaytotalagain").innerHTML = total;
    }




    // step 1

    render(){
        return(

            <form onSubmit = {this.handleSubmit}>

                
                Product ID:
                <input type="text" id="id" value={this.state.value} onChange={this.handleChange} /><br></br><br></br>

                
                Product Name:
                <input type="text" id="name" value2={this.state.value2} onChange={this.handleChange2} /><br></br><br></br>

                
                Product Category:
                <input type="text" id="category" value3={this.state.value3} onChange={this.handleChange3} /><br></br><br></br>
               

                
                Unit Price:
                <input type="text" id="unitprice" value4={this.state.value4} onChange={this.handleChange4} /><br></br><br></br>

               
                Quantity:
                <input type="text" id="quantity" value5={this.state.value5} onChange={this.handleChange5} /><br></br><br></br>

                
                Total: <span id="displaytotal"></span><br></br>
                

                <input type="submit" value="Submit"/>
                <br></br><br></br>
                Product ID: <span id="displayid"></span><br></br>
                Product Name: <span id="displayname"></span><br></br>
                Product Category: <span id="displaycategory"></span><br></br>
                Unit price: <span id="displayunitprice"></span><br></br>
                Quantity: <span id="displayquantity"></span><br></br>
                Total: <span id="displaytotalagain"></span><br></br>


            </form>
        );
    }


}


function Issue13(){
    return(
        <div>
            <Theproductdata></Theproductdata>
        </div>

    )
}

export default Issue13;