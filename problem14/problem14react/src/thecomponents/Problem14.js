import React, {useState, useEffect} from 'react';
import axios from 'axios';

function RetrieveData(){

    var [posts, setposts] = useState([])
    const url = "https://jsonplaceholder.typicode.com/posts";

    // fetch and set the data
    const fetchData = async () => {
        const {data} = await axios.get(url)
        // console.log(data)
           setposts(data)
    }

    // execute a function using useEffects. 
    useEffect(() => {
        
        document.title = 'Hello, Problem14';
        fetchData()
    })

    return (
        <div>
            <ul>
            {
                
                posts.map(post => (<p key={post.id}><b>Title:   </b>{post.title}<br></br><b>Body:   </b>{post.body}</p>))
            
            }
            </ul>
        </div>
     )
      


}

export default function Problem14(){

    return(

        <div>
            
            <RetrieveData></RetrieveData>
        </div>

    )

}