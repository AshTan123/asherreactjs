import EmailIcon from '@material-ui/icons/Email';
import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import CreateIcon from '@material-ui/icons/Create';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import {useEffect, useState } from "react";


function Removebackground(){
    document.body.style.background = "url() 0% fixed";
}

const useStyles = makeStyles((theme) => ({

    avatar: {
      margin: theme.spacing(25,85),
      backgroundColor: theme.palette.secondary.main,
      position: 'absolute',
      left: 0,
      right: 0,
      height: 200,
      width: 200,
   
    },

    emailsent:{
        margin: theme.spacing(50,90.8),
        color: 'black',
        position: 'absolute',
        left: 0,
        right: 0,
       

    },

    wehavesentyouemail:{
        margin: theme.spacing(51,68),
        color: 'black',
        position: 'absolute',
        left: 0,
        right: 0,
    },

    tryagain:{
        margin: theme.spacing(53,71),
        color: 'black',
        position: 'absolute',
        left: 0,
        right: 0,
    },

    icon:{
        height: 170,
        width: 170,
    }
    
   
 
  }));

export default function Email(){
    const classes = useStyles();
    <Removebackground />

    return(
 
        <body id="emailcontainer">
        <Avatar className={classes.avatar} >
            <EmailIcon className={classes.icon} />
        </Avatar>
        <br></br>
        <span className={classes.emailsent}><b>Email Sent</b></span> 
        <br></br>

        <span className={classes.wehavesentyouemail}>We have sent you an email with a link to reset your password </span>
        <span className={classes.tryagain}>Didnt get the email or not your email address?<Link href="/forgetpassword">try again</Link></span>
        </body>
    );
}