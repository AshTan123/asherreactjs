import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import CreateIcon from '@material-ui/icons/Create';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import {useEffect, useState } from "react";






function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}


function Changetofaint(){

    const classes = useStyles();

    var [userChoice, setChoice] = useState("Sign In")


    // what is the use of useEffect???
    useEffect(() =>{

        document.title = "Optimum Digibank";
        console.log("Use effect ran");
        console.log(userChoice);

        if(userChoice === "SignIn"){
            document.getElementById("theregister").style.color = "grey";
            document.getElementById("thesignin").style.color = "black";
            document.getElementById("theform").innerHTML = "<p class="+'"title"'+">Log in</p>" +
                                                           "<input type=" +'"text"' +"placeholder=" +'"Username"' +"autofocus/>" +
                                                           "<input type=" +'"password"' +"placeholder=" +'"Password"'+" />" +
                                                           "<a href="+'"/forgetpassword"'+">Forgot your password?</a>" + 
                                                           "<button>"+
                                                           "<span class=" +'"state"'+"><b>Log in</b></span>"+
                                                           "</button> ";

        }
        else if(userChoice === "Register")
        {
            
            document.getElementById("thesignin").style.color = "grey";
            document.getElementById("theregister").style.color = "black";
            document.getElementById("theform").innerHTML =  "<p class="+'"title"'+">Register</p>" +
                                                            "<input type=" +'"text"' +"placeholder=" +'"Full Name"' +"autofocus/>" +
                                                            "<input type=" +'"text"' +"placeholder=" +'"Email Address"' +"autofocus/>"+
                                                            "<input type=" +'"password"' +"placeholder=" +'"Password"'+" />" +
                                                            "<input type=" +'"password"' +"placeholder=" +'"Retype Password"'+" />" + 
                                                            "<button>"+
                                                            "<span class=" +'"state"'+"><b>Register</b></span>"+
                                                            "</button> ";;
        }
        
    })

    return(

       
        
       
        <Grid container>
        
         <Grid item xs={6} align="center" onClick = {() => setChoice (userChoice="SignIn")} id="thesignin" >
            <Avatar className={classes.avatar} >
                    <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5"  >
                Sign In
            </Typography>
         </Grid>
         
         
          
         
         <Grid Item xs={6} align="center"  onClick = {() => setChoice (userChoice="Register")} id="theregister" href="/register">
            <Avatar className={classes.avatar}>
            <CreateIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
                Register
            </Typography>
         </Grid>
         
        
         </Grid>
        

        

        
    )
}

const useStyles = makeStyles((theme) => ({
    root: {
      height: '100vh',
      backgroundColor: 'orange',
    },
    image: {
      backgroundImage: 'url(https://blog-www.pods.com/wp-content/uploads/2019/04/MG_1_1_New_York_City-1.jpg)',
      backgroundRepeat: 'no-repeat',
      backgroundColor:
        theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    },
    paper: {
      margin: theme.spacing(8, 4),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      
      
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
     
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));



export default function SignInSide() {
  const classes = useStyles();

  return (
    <Grid container component="main" className={classes.root} >
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square style={{backgroundColor: "Orange"}}>
     <div className={classes.paper}>
        <h2>Optimum DigiBank</h2>
        
        <Changetofaint/>

        <form className={classes.form} noValidate id="theform">
        <p class="title">Log in</p>
        <input type="text" placeholder="Username" autofocus/>
        <input type="password" placeholder="Password" /> 
        <a href="/forgetpassword">Forgot your password?</a>
        <button>
         <span class="state"><b>Log in</b></span>
        </button>
        </form>

        </div>
      </Grid>
    </Grid>
  );
}