import React, {Component } from 'react'
import Example1 from './components/Example1';
import Example3 from './components/Example3';
import Example4 from './components/Example4';
import Example5 from './components/Example5';
import Example6 from './components/Example6';
import Example7 from './components/Example7';
import Example8 from './components/Example8';
import Example9 from './components/Example9';
import HanzExample from './components/HanzExample';
import CountApp from './components/CountApp';
import UseEffectsDemo from './components/UseEffectsDemo';
import Example10 from './components/Example10';
import Example11 from './components/Example11';
import Example12 from './components/Example12';

function App() {
    let name1 = "variable can be re-assigned";
    const name2 = "variable won't be re-assigned";
    var name3 = "mostly preferable by the developers"
      return(
        <div>
            <h1>{name1}</h1>
            <h2>{name2}</h2>
            <h3>{name3}</h3>
            <Hello1></Hello1>
            <Example1></Example1>
            <Example2></Example2>
            <Example3></Example3>
            <Example4></Example4>
            <Example5></Example5>
            <Example6></Example6>
            <Example7></Example7>
            <Example8></Example8>
            <Example9></Example9>
            <HanzExample></HanzExample>
            <CountApp></CountApp>
            <UseEffectsDemo></UseEffectsDemo>
            <Example10></Example10>
            <Example11></Example11>
            <Example12></Example12>
        </div>
    )
  
}

class Hello1 extends Component{
 
  render(){
    return(
      <div>
        <p>Greetings.. from class Hello</p>
      </div>
    )
  }
}


function Example2(){
  return(
      <div>
          <DataLogic1></DataLogic1>
          <DataLogic2></DataLogic2>
      </div>
  )
}


// functional component
function DataLogic1(){
  var name1 = "variable can be re-assigned";
  const name2 = "variable won't be re-assigned";
  return(
      <div>
          <h1>{name1}</h1>
          <h2>{name2}</h2>
      </div>
  )
}


// class component
var DataLogic2 = () => {
  function sayHello(){
      alert('hello, World');
}

return(
  <button onClick={sayHello}> Click Here!</button>
  );
};

export default App;