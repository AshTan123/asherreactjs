import React, { useState } from 'react';  

  
function CountApp() {  

  // Declare a new state variable, which we'll call "count"  
  
  // count is the variable, setCount is assigning the value to count
  const [count, setCount] = useState(0);  
  
  return (  
    <div>  
      <p>You clicked {count} times</p>  
      <button onClick={() => setCount(count + 1)}>  
        Click me  
      </button>  
    </div>  
  );  
} 


export default CountApp;  