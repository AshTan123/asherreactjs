import React from 'react';

// functional component
function DataLogic1(){
    var name1 = "Example1 ==> DataLogic1() ==> variable can be re-assigned";
    const name2 = "Example1 ==> DataLogic1() ==> variable won't be re-assigned";
    return(
        <div>
            <h1>{name1}</h1>
            <h2>{name2}</h2>
        </div>
    )
}

// class component
var DataLogic2 = () => {
    function sayHello(){
        alert('Example1 ==> DataLogic2() ==> hello, World');
}

return(
    <button onClick={sayHello}> Click Here!</button>
    );
};

function Example1(){
    return(
        <div>
            <DataLogic1></DataLogic1>
            <DataLogic2></DataLogic2>
        </div>
    )
}

export default Example1;
