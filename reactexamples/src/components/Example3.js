
import React from 'react';

function Example3(){

    var userInfor = "Hello from Example3";
    return <Logic value={userInfor}/>

}

function Logic(props){
    return <h1>{props.value}</h1>
}

export default Example3;