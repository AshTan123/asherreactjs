// take an input from user and display to the browser

import React from 'react'

class UserData extends React.Component{

    // step 2 : add a constructor

    constructor(props){
        super(props);
        this.state = {value: ''};
    }

    // step 3 : add handlechange

    handleChange = event => {
        this.setState({value : event.target.value});
    }

    // step 4:
    handleSubmit(event){
        console.log('Hello ' +this.state.value);
        var data = document.getElementById("id1").value;
        document.getElementById("output").innerHTML = data;
        event.preventDefault();
    }


    // step 1: take input from the user

    render(){
        return(

            <form onSubmit = {this.handleSubmit}>

                User Name:
                <input type="text" id="id1" value={this.state.value} onChange={this.handleChange} />
                <input type="submit" value="Submit"/>
                <br></br><span id="output"></span>

            </form>
        );
    }

}

function Example6(){
    return(
        <div>
            <UserData></UserData>
        </div>

    )
}

export default Example6;