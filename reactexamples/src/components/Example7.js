
// React JS 16.8 version onwards we have Hooks concept.
import {useEffect, useState } from "react";


// useEffect and useState

// re-usable component

function GetData1(){

    var [count, setCount] = useState(0)

    // what is the use of useEffect??? the outcome caused by the change of the use state variable
    useEffect(() =>{
        document.title = {count}
        console.log("Use effect ran");
    })

    return(
        <div>

            <p> You have clicked {count} times</p>
            <button onClick = {() => setCount (count+2)} > Click here </button>

        </div>
    )

}

export default function Example7 (){

    return(

        <div>
            <GetData1></GetData1>
        </div>

    )

}



