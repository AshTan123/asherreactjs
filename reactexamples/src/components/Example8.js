
import {useEffect, useState } from "react";
import axios from "axios"
import React from 'react';

//axios

function GetData2(){

    // set posts is not use as we did not set any value to posts
    var [posts, setposts] = useState([])

    // on load document, show the data of JSON in console. Axios links the API to the frontend react
    useEffect(() =>{
        axios.get('https://jsonplaceholder.typicode.com/posts')
        .then(res => {
            console.log(res); // res meaning response
            
        })
        .catch(err => {
            console.log(err);
        })
    })

    return(

        <div>
            <ul>
                {
                    // display the title --> key is the id and title is the output
                    posts.map(post => <p key ={post.id} > {post.title} </p>)
                    
                }
            </ul>
        </div>

    )
}



export default function Example8 (){

    return(

        <div>
            <GetData2></GetData2>
        </div>


    )

}

