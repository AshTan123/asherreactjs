import React, {useState} from 'react';

// This example of hooks is use to demostrate the use of hooks when 2 buttons affects the value of a variable count
var GetData3 = () => {

    var [count, setCount] = useState(0);

    var handleIncrement = () =>

        setTimeout(
            () => setCount(currentCount => currentCount +2),
            100
        );

        var handleDecrement = () =>

        setTimeout(
            () => setCount(currentCount => currentCount -2),
            100
        );

        return(
           
            <div>
                <h1>{count}</h1>
                <Button handleClick = {handleIncrement}>Increment</Button>
                <Button handleClick = {handleDecrement}>Decrement</Button>
            </div>
        );

};


var Button = ({handleClick, children}) => (
    <button type = "button" onClick={handleClick}>
    {children}
    </button>
);


export default function Example9(){

    return(

        <div>
            <GetData3></GetData3>
        </div>

    )

}