import React, {useState, useEffect} from 'react'
import axios from 'axios';
 
export default function Post() {
 
 const [posts, setPosts] = useState([]);
 const url = "https://jsonplaceholder.typicode.com/posts";
 
 const fetchData = async () => {
 const {data} = await axios.get(url)
 // console.log(data)
    setPosts(data)
 }
 
 useEffect(() => {
    fetchData()
 })
 
 return (
    <div>
       <ul>
        {
            posts.map(post => (<p key={post.id}><b>Title:  </b>{post.title}<br></br><b>Body:   </b>{post.body}</p>))
        
        }
        </ul>
    </div>
 )
}