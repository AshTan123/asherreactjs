import React, { useState, useEffect } from 'react';  
  
function UseEffectsDemo() {  
  const [count, setCount] = useState(0);  
  
  // Similar to componentDidMount and componentDidUpdate:  

  
  // useEffect means that on run function, show the effect 
  useEffect(() => {  
    // Update the document title using the browser API  
    document.title = 'You clicked ' +count +" times.";  
  });  
  
  return (  
    <div>  
      <p>You clicked {count} times</p>  
      <button onClick={() => setCount(count + 1)}>  
        Click me  
      </button>  
    </div>  
  );  
}  
export default UseEffectsDemo;  